```matlab
%%
nImages = 650;
figure;
for idx = 1:nImages
    x=[];
    n=1000;
    k=1.39+idx/1000;
    x(1)=0.54321;
    for i=2:n
        x(i)=k*x(i-1)^2-1;
    end
    plot(x)
    title('Easy Chaos: x_i=k*x_(_i_-_1_)^2-1')
    ylim([-1 1]);
    annotation('textbox',[.15 .6 .3 .3],'String',{'k=' k},'FontSize',12,'FitBoxToText','on');
    drawnow
    frame = getframe(1);
    im{idx} = frame2im(frame);
    close;
end

%%
filename = 'easy_chaos.gif';
for idx = 1:nImages
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0);
    end
end
```

k=1 - order

k=1.4 - cycle through 32 values

k=1.7 - chaos settles in

k=1.74 - CHAOS

k=1.75 - order

k=2 - chaos again


![Easy Chaos](easy_chaos.gif)